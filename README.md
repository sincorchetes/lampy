```
                                             ..
                                           dP/$.
                                           $4$$%
                                         .ee$$ee.
                                    .eF3??????$C$r.        .d$$$$$$$$$$$e.
.zeez$$$$$be..                    JP3F$5'$5K$?K?Je$.     d$$$FCLze.CC?$$$e
    """??$$$$$$$$ee..         .e$$$e$CC$???$$CC3e$$$$.  $$$/$$$$$$$$$.$$$$
           `"?$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$b $$"$$$$P?CCe$$$$$F
                "?$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$b$$J?bd$$$$$$$$$F"
                    "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$d$$F"
                       "?$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"...
                           "?$$$$$$$$$$$$$$$$$$$$$$$$$F "$$"$$$$b
                               "?$$$$$$$$$$$$$$$$$$F"     ?$$$$$F
                                    ""????????C"
                                    e$$$$$$$$$$$$.
                                  .$b CC$????$$F3eF
                                4$bC/%$bdd$b@$Pd??Jbbr
                                 ""?$$$$eeee$$$$F?"'
       ASCII art: https://textart.io/art/tag/magic-lamp/1

```
## Introduction
Lampy is a script to perform a less setup of LAMP or LNMP with extended features likes FTP, edit main configuration files of services and more that's comming in the next version.

## License
Lampy is licensed under GPL v2.0 terms.

## Where I can use Lampy?
You can setup your own LAMP+/LNMP+ in this distributions:
* Archlinux
* Debian
* Fedora
* openSUSE Tumbleweed/Leap
* Ubuntu

If you wanna get support of your distro, please contact me to:
```
contacto <at> echemosunbitstazo <dot> es
```
