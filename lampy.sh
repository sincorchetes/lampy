#!/bin/bash
######################################################
#		WELCOME TO LAMPY SCRIPT V1.0
#                      	                    ..
#                                          dP/$.
#                     	                   $4$$%
#                   	                  .ee$$ee.
#                                    .eF3??????$C$r.        .d$$$$$$$$$$$e.
#.zeez$$$$$be..                    JP3F$5'$5K$?K?Je$.     d$$$FCLze.CC?$$$e
#    """??$$$$$$$$ee..         .e$$$e$CC$???$$CC3e$$$$.  $$$/$$$$$$$$$.$$$$
#           `"?$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$b $$"$$$$P?CCe$$$$$F
#                "?$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$b$$J?bd$$$$$$$$$F"
#                    "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$d$$F"
#                       "?$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"...
#                           "?$$$$$$$$$$$$$$$$$$$$$$$$$F "$$"$$$$b
#                               "?$$$$$$$$$$$$$$$$$$F"     ?$$$$$F
#                                    ""????????C"
#                                    e$$$$$$$$$$$$.
#                                  .$b CC$????$$F3eF
#                                4$bC/%$bdd$b@$Pd??Jbbr
#                                 ""?$$$$eeee$$$$F?"'
#	ASCII art: https://textart.io/art/tag/magic-lamp/1
#
######################################################
# How to contact?
#
# Álvaro Castillo 
# E-mail: contacto <at> echemsounbitstazo <dot> com
# Twitter, GitLab, Vivaldi and more social accounts: @sincorchetes
# Website: https://echemsounbitstazo.es
#
######################################################

SYSTEMS=("fedora" "debian" "ubuntu" "arch" "opensuse")
CURRENT_SYS=`grep -w "ID" /etc/os-release |cut -f 2 -d "="`

echo "Welcome to Lampy v.1.0.0

This script is licensed under GPLv2.0 terms.

Choose what's services do you wanna setup into your system?:

### Apache HTTP ###
1) Web Server
2) Web Server + FTP (vsftpd)
3) Web Server + MariaDB
4) All of them

### Nginx  ###
5) Ngix Web server
6) Ngix Web serv + FTP (vsftpd)
7) Nginx + FTP (vsftpd) + MariaDB
8) Web Server + MariaDB
9) All of them"

read -p "Choose one: " SERVICES 

	if [ ${CURRENT_SYS} == ${SYSTEMS[0]} ]
	then
		case $SERVICES in
			1) 
			echo "This script will be ask root password: "
			su -c "dnf install -y httpd && systemctl start httpd && systemctl enable httpd"
			if [ $? -eq 0 ]
			then
				echo "Apache was installed successfully."
				systemctl status httpd.service |grep active
			else
				echo "Something wrong"
			fi
			;;

			2) 
			echo "This script will be ask root password: "
			su -c "dnf install -y httpd vsftpd && systemctl start httpd vsftpd && systemctl enable httpd vsftpd"
			if [ $? -eq 0 ]
			then
				echo "Apache and vsftpd (FTP) was installed successfully."
				systemctl status httpd.service |grep active
				systemctl status vsftpd |grep active
			else
				echo "Something wrong"
			fi
			;;

			3) 
			echo "This script will be ask root password: "
			su -c "dnf install -y httpd vsftpd mariadb-server && systemctl start httpd vsftpd mariadb && systemctl enable httpd vsftpd mariadb"
			if [ $? -eq 0 ]
			then
				echo "Apache and MariaDB was installed successfully."
				systemctl status httpd.service |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"

				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			3) 
			echo "This script will be ask root password: "
			su -c "dnf install -y httpd mariadb-server && systemctl start httpd mariadb && systemctl enable httpd mariadb"
			if [ $? -eq 0 ]
			then
				echo "Apache and MariaDB was installed successfully."
				systemctl status httpd.service |grep active
				systemctl status vsftpd |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"
				
				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			4) 
			echo "This script will be ask root password: "
			su -c "dnf install -y httpd vsftpd mariadb-server && systemctl start httpd vsftpd mariadb && systemctl enable httpd vsftpd mariadb"
			if [ $? -eq 0 ]
			then
				echo "Apache, vsftpd (FTP) and MariaDB was installed successfully."
				systemctl status httpd.service |grep active
				systemctl status vsftpd |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"
				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			5) 
			echo "This script will be ask root password: "
			su -c "dnf install -y nginx && systemctl start nginx && systemctl enable nginx"
			if [ $? -eq 0 ]
			then
				echo "Nginx was installed successfully."
				systemctl status nginx.service |grep active
			else
				echo "Something wrong"
			fi
			;;

			6) 
			echo "This script will be ask root password: "
			su -c "dnf install -y nginx vsftpd && systemctl start nginx vsftpd && systemctl enable nginx vsftpd"
			if [ $? -eq 0 ]
			then
				echo "Nginx and vsftpd (FTP) was installed successfully."
				systemctl status nginx.service |grep active
				systemctl status vsftpd |grep active
			else
				echo "Something wrong"
			fi
			;;

			7) 
			echo "This script will be ask root password: "
			su -c "dnf install -y nginx vsftpd mariadb-server && systemctl start nginx vsftpd mariadb && systemctl enable nginx vsftpd mariadb"
			if [ $? -eq 0 ]
			then
				echo "Nginx and MariaDB was installed successfully."
				systemctl status nginx.service |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"

				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			8) 
			echo "This script will be ask root password: "
			su -c "dnf install -y nginx mariadb-server && systemctl start nginx mariadb && systemctl enable nginx mariadb"
			if [ $? -eq 0 ]
			then
				echo "Nginx and MariaDB was installed successfully."
				systemctl status nginx.service |grep active
				systemctl status vsftpd |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"
				
				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			9) 
			echo "This script will be ask root password: "
			su -c "dnf install -y nginx vsftpd mariadb-server && systemctl start nginx vsftpd mariadb && systemctl enable nginx vsftpd mariadb"
			if [ $? -eq 0 ]
			then
				echo "Nginx, vsftpd (FTP) and MariaDB was installed successfully."
				systemctl status nginx.service |grep active
				systemctl status vsftpd |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"
				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			*)
				echo "Please choose one option for the next time ;-)"
			;;
		esac
######### END FEDORA ###########
#																   # 
### STARTED DEBIAN & UBUNTU c#####

	elif [ ${CURRENT_SYS} == ${SYSTEMS[1]} ] || [ ${CURRENT_SYS} == ${SYSTEMS[2]} ] 
		then
			case $SERVICES in
			1) 
			echo "This script will be ask root password: "
			sudo apt-get install -y apache2 && systemctl start apache.service && systemctl enable apache.service
			if [ $? -eq 0 ]
			then
				echo "Apache was installed successfully."
				systemctl status apache.service |grep active
			else
				echo "Something wrong"
			fi
			;;

			2) 
			echo "This script will be ask root password: "
			sudo apt-get install -y apache2 vsftpd && systemctl start apache.service vsftpd.service && systemctl enable apache2 vsftpd
			if [ $? -eq 0 ]
			then
				echo "Apache and vsftpd (FTP) was installed successfully."
				systemctl status apache2.service |grep active
				systemctl status vsftpd |grep active
			else
				echo "Something wrong"
			fi
			;;

			3) 
			echo "This script will be ask root password: "
			sudo apt-get install -y apache2 vsftpd mariadb-server && systemctl start apache.service vsftpd.service mariadb.service && systemctl enable apache2 vsftpd mariadb
			if [ $? -eq 0 ]
			then
				echo "Apache and MariaDB was installed successfully."
				systemctl status apache2.service |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				sudo mysql_secure_installation

				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			3) 
			echo "This script will be ask root password: "
			sudo apt-get install -y apache2 mariadb-server && systemctl start apache.service mariadb && systemctl enable apache2 mariadb
			if [ $? -eq 0 ]
			then
				echo "Apache and MariaDB was installed successfully."
				systemctl status apache2.service |grep active
				systemctl status vsftpd |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				sudo  mysql_secure_installation
				
				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			4) 
			echo "This script will be ask root password: "
			sudo apt-get install -y apache2 vsftpd mariadb-server && systemctl start apache.service vsftpd.service mariadb.service && systemctl enable apache2 vsftpd mariadb
			if [ $? -eq 0 ]
			then
				echo "Apache, vsftpd (FTP) and MariaDB was installed successfully."
				systemctl status apache2.service |grep active
				systemctl status vsftpd |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				sudo mysql_secure_installation
				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			5) 
			echo "This script will be ask root password: "
			sudo apt-get install -y nginx && systemctl start nginx && systemctl enable nginx
			if [ $? -eq 0 ]
			then
				echo "Nginx was installed successfully."
				systemctl status nginx.service |grep active
			else
				echo "Something wrong"
			fi
			;;

			6) 
			echo "This script will be ask root password: "
			sudo apt-get install -y nginx vsftpd && systemctl start nginx vsftpd && systemctl enable nginx vsftpd
			if [ $? -eq 0 ]
			then
				echo "Nginx and vsftpd (FTP) was installed successfully."
				systemctl status nginx.service |grep active
				systemctl status vsftpd |grep active
			else
				echo "Something wrong"
			fi
			;;

			7) 
			echo "This script will be ask root password: "
			sudo apt-get install -y nginx vsftpd mariadb-server && systemctl start nginx vsftpd mariadb && systemctl enable nginx vsftpd mariadb
			if [ $? -eq 0 ]
			then
				echo "Nginx and MariaDB was installed successfully."
				systemctl status nginx.service |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				sudo mysql_secure_installation

				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			8) 
			echo "This script will be ask root password: "
			sudo apt-get install -y nginx mariadb-server && systemctl start nginx mariadb && systemctl enable nginx mariadb
			if [ $? -eq 0 ]
			then
				echo "Nginx and MariaDB was installed successfully."
				systemctl status nginx.service |grep active
				systemctl status vsftpd |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				sudo mysql_secure_installation
				
				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			9) 
			echo "This script will be ask root password: "
			sudo  apt-get install -y nginx vsftpd mariadb-server && systemctl start nginx vsftpd mariadb && systemctl enable nginx vsftpd mariadb
			if [ $? -eq 0 ]
			then
				echo "Nginx, vsftpd (FTP) and MariaDB was installed successfully."
				systemctl status nginx.service |grep active
				systemctl status vsftpd |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				sudo mysql_secure_installation
				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			*)
				echo "Please choose one option for the next time ;-)"
			;;
		esac

### END DEBIAN & UBUNTU ####

### STARTED ARCHLINUX #####

	elif [ ${CURRENT_SYS} == ${SYSTEMS[3]} ] 
		then
			case $SERVICES in
			1) 
			echo "This script will be ask root password: "
			su -c "pacman -S --noconfirm apache && systemctl start apache.service && systemctl enable apache.service"
			if [ $? -eq 0 ]
			then
				echo "Apache was installed successfully."
				systemctl status apache.service |grep active
			else
				echo "Something wrong"
			fi
			;;

			2) 
			echo "This script will be ask root password: "
			su -c "pacman -S --noconfirm apache vsftpd && systemctl start apache vsftpd && systemctl enable apache vsftpd"
			if [ $? -eq 0 ]
			then
				echo "Apache and vsftpd (FTP) was installed successfully."
				systemctl status apache.service |grep active
				systemctl status vsftpd |grep active
			else
				echo "Something wrong"
			fi
			;;

			3) 
			echo "This script will be ask root password: "
			su -c "pacman -S --noconfirm apache vsftpd mariadb && systemctl start apache vsftpd mariadb && systemctl enable apache.service vsftpd.service mariadb.service"
			if [ $? -eq 0 ]
			then
				echo "Apache and MariaDB was installed successfully."
				systemctl status apache.service |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"

				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			3) 
			echo "This script will be ask root password: "
			su -c "pacman -S --noconfirm apache mariadb && systemctl start apache mariadb && systemctl enable apache mariadb"
			if [ $? -eq 0 ]
			then
				echo "Apache and MariaDB was installed successfully."
				systemctl status apache.service |grep active
				systemctl status vsftpd |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"
				
				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			4) 
			echo "This script will be ask root password: "
			su -c "pacman -S --noconfirm apache vsftpd mariadb && systemctl start apache vsftpd mariadb && systemctl enable apache.service vsftpd.service mariadb.service"
			if [ $? -eq 0 ]
			then
				echo "Apache, vsftpd (FTP) and MariaDB was installed successfully."
				systemctl status apache.service |grep active
				systemctl status vsftpd |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"
				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			5) 
			echo "This script will be ask root password: "
			su -c "pacman -S --noconfirm nginx && systemctl start nginx && systemctl enable nginx"
			if [ $? -eq 0 ]
			then
				echo "Nginx was installed successfully."
				systemctl status nginx.service |grep active
			else
				echo "Something wrong"
			fi
			;;

			6) 
			echo "This script will be ask root password: "
			su -c "pacman -S --noconfirm nginx vsftpd && systemctl start nginx vsftpd && systemctl enable nginx vsftpd"
			if [ $? -eq 0 ]
			then
				echo "Nginx and vsftpd (FTP) was installed successfully."
				systemctl status nginx.service |grep active
				systemctl status vsftpd |grep active
			else
				echo "Something wrong"
			fi
			;;

			7) 
			echo "This script will be ask root password: "
			su -c "pacman -S --noconfirm nginx vsftpd mariadb && systemctl start nginx vsftpd mariadb && systemctl enable nginx vsftpd mariadb"
			if [ $? -eq 0 ]
			then
				echo "Nginx and MariaDB was installed successfully."
				systemctl status nginx.service |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"

				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			8) 
			echo "This script will be ask root password: "
			su -c "pacman -S --noconfirm nginx mariadb && systemctl start nginx mariadb && systemctl enable nginx mariadb"
			if [ $? -eq 0 ]
			then
				echo "Nginx and MariaDB was installed successfully."
				systemctl status nginx.service |grep active
				systemctl status vsftpd |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"
				
				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			9) 
			echo "This script will be ask root password: "
			su -c "pacman -S --noconfirm nginx vsftpd mariadb && systemctl start nginx vsftpd mariadb && systemctl enable nginx vsftpd mariadb"
			if [ $? -eq 0 ]
			then
				echo "Nginx, vsftpd (FTP) and MariaDB was installed successfully."
				systemctl status nginx.service |grep active
				systemctl status vsftpd |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"
				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			*)
				echo "Please choose one option for the next time ;-)"
			;;
		esac
### END ARCHLINUX ####

### START OPENSUSE #####
elif [ ${CURRENT_SYS} == ${SYSTEMS[4]} ] 
		then
			case $SERVICES in
			1) 
			echo "This script will be ask root password: "
			su -c "zypper in -y apache2 php7 && systemctl start apache.service && systemctl enable apache.service"
			if [ $? -eq 0 ]
			then
				echo "Apache was installed successfully."
				systemctl status apache.service |grep active
			else
				echo "Something wrong"
			fi
			;;

			2) 
			echo "This script will be ask root password: "
			su -c "zypper in -y apache2 vsftpd php7 && systemctl start apache vsftpd && systemctl enable apache vsftpd"
			if [ $? -eq 0 ]
			then
				echo "Apache and vsftpd (FTP) was installed successfully."
				systemctl status apache.service |grep active
				systemctl status vsftpd |grep active
			else
				echo "Something wrong"
			fi
			;;

			3) 
			echo "This script will be ask root password: "
			su -c "zypper in -y apache2 vsftpd mariadb php7 && systemctl start apache vsftpd mariadb && systemctl enable apache.service vsftpd.service mariadb.service"
			if [ $? -eq 0 ]
			then
				echo "Apache and MariaDB was installed successfully."
				systemctl status apache.service |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"

				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			3) 
			echo "This script will be ask root password: "
			su -c "zypper in -y apache2 mariadb php7 && systemctl start apache mariadb && systemctl enable apache mariadb"
			if [ $? -eq 0 ]
			then
				echo "Apache and MariaDB was installed successfully."
				systemctl status apache.service |grep active
				systemctl status vsftpd |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"
				
				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			4) 
			echo "This script will be ask root password: "
			su -c "zypper in -y apache2 vsftpd mariadb php7 && systemctl start apache vsftpd mariadb && systemctl enable apache.service vsftpd.service mariadb.service"
			if [ $? -eq 0 ]
			then
				echo "Apache, vsftpd (FTP) and MariaDB was installed successfully."
				systemctl status apache.service |grep active
				systemctl status vsftpd |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"
				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			5) 
			echo "This script will be ask root password: "
			su -c "zypper in -y nginx php7 && systemctl start nginx && systemctl enable nginx"
			if [ $? -eq 0 ]
			then
				echo "Nginx was installed successfully."
				systemctl status nginx.service |grep active
			else
				echo "Something wrong"
			fi
			;;

			6) 
			echo "This script will be ask root password: "
			su -c "zypper in -y  nginx vsftpd php7 && systemctl start nginx vsftpd && systemctl enable nginx vsftpd"
			if [ $? -eq 0 ]
			then
				echo "Nginx and vsftpd (FTP) was installed successfully."
				systemctl status nginx.service |grep active
				systemctl status vsftpd |grep active
			else
				echo "Something wrong"
			fi
			;;

			7) 
			echo "This script will be ask root password: "
			su -c "zypper in -y nginx vsftpd mariadb php7 && systemctl start nginx vsftpd mariadb && systemctl enable nginx vsftpd mariadb"
			if [ $? -eq 0 ]
			then
				echo "Nginx and MariaDB was installed successfully."
				systemctl status nginx.service |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"

				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			8) 
			echo "This script will be ask root password: "
			su -c "zypper in -y  nginx mariadb php7 && systemctl start nginx mariadb && systemctl enable nginx mariadb"
			if [ $? -eq 0 ]
			then
				echo "Nginx and MariaDB was installed successfully."
				systemctl status nginx.service |grep active
				systemctl status vsftpd |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"
				
				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			9) 
			echo "This script will be ask root password: "
			su -c "zypper in -y  nginx vsftpd mariadb php7 && systemctl start nginx vsftpd mariadb && systemctl enable nginx vsftpd mariadb"
			if [ $? -eq 0 ]
			then
				echo "Nginx, vsftpd (FTP) and MariaDB was installed successfully."
				systemctl status nginx.service |grep active
				systemctl status vsftpd |grep active
				systemctl status mariadb.service |grep active
				echo "Just only need one step to finish this setup. Setting up MariaDB server:"
				su -c "mysql_secure_installation"
				if [ $? -eq 0 ]
				then
					echo "Setup was finished. Enjoy ;-)"
				else
					echo "Something wrong"
				fi
			else
				echo "Something wrong"
			fi
			;;

			*)
				echo "Please choose one option for the next time ;-)"
			;;
		esac
	else
		echo "Your distro is not currently supported. If you wish supported, please contact me at <contacto at echemsounbitstazo dot es>"
	fi

